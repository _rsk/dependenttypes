module Odd

public export

data Odd : Nat -> Type where
 MkOdd : (k : Nat) -> {auto p : n = S (k + k)} -> Odd n
