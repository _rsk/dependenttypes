module SortedList

public export

data SortedList : Nat -> Type where
    Nil  : SortedList 0
    (::) : (x : Nat) -> SortedList y -> {auto p : LTE y x} -> SortedList x
