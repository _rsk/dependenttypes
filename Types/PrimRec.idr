module PRec

import Data.Vect
import Data.Fin

-- Datatype for primitive recursive function where f : PRFun n means
-- that f is an n-ary primitive recursive function
data PRFun : Nat -> Type where

-- Constant function
    Const : (n: Nat) -> Nat -> PRFun n
    --      ^ arity ^constant

-- Successor function is a 1-ary p.r. function
    Succ : PRFun (S Z)

-- Projection
    Projection : (n : Nat) -> Fin n -> PRFun n
    --            ^ arity     ^ index to project o

-- Composition
    Compose : PRFun k -> Vect k (PRFun n) -> PRFun n

-- Primitive recursion
    PRec : PRFun k -> PRFun (S (S k)) -> PRFun (S k)


-- Evaluating primitive recursive functions
eval : PRFun n -> Vect n Nat -> Nat

eval (Const n m) _ = m

eval  Succ [x] = S x

eval (Projection n k) xs  = index k xs

eval (Compose f gs) xs = eval f (map (flip eval xs) gs)

eval (PRec c g) (Z :: xs) = eval c xs
eval f@(PRec c g) ((S n) :: xs) = eval g $ (eval f (n :: xs))::n::xs


------------------------------------------------------------------------
-- Examples
------------------------------------------------------------------------

addition : PRFun 2
addition = PRec c g
    where
        c = Projection 1 0
        g = Compose Succ [Projection 3 0]

multiplication : PRFun 2
multiplication = PRec c g
    where
        c = Const 1 Z
        g = Compose addition [Projection 3 0, Projection 3 2]

rPower : PRFun 2
rPower = PRec c g
    where
        c = Const 1 1
        g = Compose multiplication [Projection 3 0, Projection 3 2]

