module Even

public export

data Even : Nat -> Type where
    MkEven : (k : Nat) -> {auto p : n = k + k} -> Even n
