# Dependent Types

This repository contains a collection of dependent types written in the programming language Idris (https://www.idris-lang.org/).

## Build and use

Build with the standard build tools of Idris:

```bash
$ git clone https://gitlab.com/_rsk/dependenttypes
$ cd dependenttypes
$ idris --build DependentTypes.ipkg
$ idris *.ibc
```

## Even Type

The type *Even n* ensures that *n* is an even natural number.
```haskell
data Even : Nat -> Type where
    MkEven : (k : Nat) -> {auto p : n = k + k} -> Even n
```

For every even number *n* a number *k* exisists so that `n = k + k` is true. 
The proof for *Even n* is the correspondent number *k*:
```haskell
> Even Type
Even : Nat -> Type
> MkEven 0
MkEven 0 : Even 0
> MkEven 1
MkEven 1 : Even 2
> MkEven 3
MkEven 3 : Even 6
```

## Odd Type

The type *Odd n* ensures that *n* is an odd natural number.
```haskell
data Odd : Nat -> Type where
 MkOdd : (k : Nat) -> {auto p : n = S (k + k)} -> Odd n
```

For every odd number *n* a number *k* exisists so that `n = k + k + 1` is true. 
The proof for *Odd n* is the correspondent number *k*:
```haskell
> Odd
Odd : Nat -> Type
> MkOdd 0
MkOdd 0 : Odd 1
> MkOdd 1
MkOdd 1 : Odd 3
> MkOdd 2
MkOdd 2 : Odd 5
> MkOdd 3
MkOdd 3 : Odd 7
```

## SortedList Type

The type *SortedList* ensures that the list of natural numbers is sorted in descending order.
```haskell
data SortedList : Nat -> Type where
    Nil  : SortedList 0
    (::) : (x : Nat) -> SortedList y -> {auto p : LTE y x} -> SortedList x
```

A *SortedList n* depends on the natural number *n* which is highest number in the list.
The proof that a new natural number *x* can be prepended to a sorted list *SortedList n* is to show that `y <= x`. 
```haskell
> 5 :: 4 :: 3 :: 2 :: 1 :: Nil
5 :: 4 :: 3 :: 2 :: 1 :: [] : SortedList 5
> 10 :: 5 :: 1 :: Nil
10 :: 5 :: 1 :: [] : SortedList 10
> SortedList.Nil
[] : SortedList 0
```

One cannot prepend a natural number which is less than the higest number in the list.
```haskell
> 1 :: 2 :: Nil
(input):1:3-4:When checking argument p to constructor SortedList.:::
        Can't find a value of type 
                LTE 2 1
```
